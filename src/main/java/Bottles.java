
import java.util.ArrayList;

class Bottles {
	public String verse(int verseNumber) {
		if (verseNumber == 99) {
			return "99 bottles of beer on the wall, " + "99 bottles of beer.\n"
					+ "Take one down and pass it around, " + "98 bottles of beer on the wall.\n";
		} else if (verseNumber == 1) {
			return "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
					+ "no more bottles of beer on the wall.\n";
		} else if (verseNumber == 2) {
			return "3 bottles of beer on the wall, " + "3 bottles of beer.\n"
					+ "Take one down and pass it around, " + "2 bottles of beer on the wall.\n";
		} else if (verseNumber == 3) {
			return "2 bottles of beer on the wall, " + "2 bottles of beer.\n"
					+ "Take one down and pass it around, " + "1 bottle of beer on the wall.\n";
		} else if (verseNumber == 0) {
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		} else {
			return Integer.toString(verseNumber) + " bottles of beer on the wall, " + Integer.toString(verseNumber) + " bottles of beer.\n"
					+ "Take one down and pass it around, " + Integer.toString(verseNumber - 1) + " bottles of beer on the wall.\n";
		}
	}


	public String verse(int startVerseNumber, int endVerseNumber) {
		ArrayList<String> res = new ArrayList<>();
		for (int i = startVerseNumber; i >= endVerseNumber; i--){
			res.add(verse(i));
		}
		return String.join("\n", res);
	}

	public String song() {
		return verse(99, 0);
	}
}
